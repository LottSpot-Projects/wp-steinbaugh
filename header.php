<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" />
	<?php } ?>

	<title>
		   <?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' Archive - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - '; bloginfo('description'); }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>
	</title>

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
	<?php wp_head(); ?>
</head>

<body>
  <div id="header">
    <div id="title-tagline-container">
    <h1 id="blog-title"><?php bloginfo('name'); ?></h1>
    <h2 id="blog-tagline"><?php bloginfo('description'); ?>
    </div><!--End title tagline container-->
    <div id="social-icons-container">
      <div id="social-buttons">
	<img src="<?php bloginfo('template_directory'); ?>/images/twitter_logo.png" height="30px" width="30px" id="twitter-button" alt="Adam on Twitter" />
	<img src="<?php bloginfo('template_directory'); ?>/images/linkedin_logo.png" heiht="30px" width="30px" id="linkedin-button" alt="Adam on LinkedIn" />
	</div><!--End social buttons-->
      <div id="social-banner">
	<img src="<?php bloginfo('template_directory'); ?>/images/rss_banner_scale.png" id="rss-banner" alt="Adam's blog RSS" />
      </div><!--End social banner-->
    </div><!--End social icons container-->
    <div id="banner">
      <img src="<?php bloginfo('template_directory'); ?>/images/tempbanner.png" alt="Just a placeholder!" />
    </div><!--End banner-->
    <?php
      wp_nav_menu(
	array(
	  'theme_location'=>'navbar',
	  'container_id'=>'navbar',
	  'fallback_cb'=>'wp_page_menu'
	)
      );
    ?>
    
    <div id="Marquee">
      <?php ptmsshow(); ?>
    </div><!--End marquee-->
    <div id="search box">
      <?php get_search_form(); ?>
    </div><!--End search box-->
  </div><!--End header-->

  <div id="page-container">
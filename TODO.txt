<?
DESIGN THOUGHTS:
Layout should be a 'newspaper' type grid design which organizes posts into "sections" of the page, depending on the category (or tags), with a most recent posts list available somewhere easily accessible by visitors. Should be a 2 column layout, meaning 2 sections; a wider column with lead story and 2 sub stories organized in 2 horizontal rows and a more narrow column laid out as 3 vetical news stories.

DESIGN PLAN:

  |=> General theme design
    |-> 'Theme template will be built for resizable maximum 1000px wide page'
    |-> Theme will be designed with two components: Out of the box functionality and extensible functionality.
      |- Out of the box functionality will be implemented in the index page, with extensible functionality implemented in site pages
    |-> Universal design components
      => Include "related stories" at bottom of single.php (probably using tags)
      => Comments visible on single.php only, with number count available on excerpt
      => Comment count available in footer of story excerpt; clicking links to single.php
      => Menus
	-> Design decision has been made to support header navigation only; plans for sidebar navigation are being dropped.
      
  |=> Index page
    |-> Provide a default layout for "out of the box" functionality.
      |- Default layout should feature news stories on left, older stories on right type of approach until plugin work can be complete
      |-> Provide support for standard widget sidebars as part of "out of the box" functionality.
    |-> Write plugin for the theme to make organization of index page content customizable, controlling which categories show up in which sections [This plugin may or may not make the 1.0 release]
      |- This plugin has been moved to its own project
      |- Work for plugin API will be integrated into theme work
	=> API will consist of variables which are plugged into wp_query function; variables will be loaded from config file.

  |=> Site pages
    |-> OPTION 1: Template-centric design
      |- Hard code category and tag sorting into verious templates
	=> Easier to implement, but will greatly degrade user experience
	=> check in on his use of categories and/or tags. This could be good information to know.
	=> Would likely hook into plugin used by index page to control content parsing, so templates can focus on varied layouts.
    |-> OPTION 2: Widget-centric desgin
      |- Create page templates which govern widget layout rather than templates with pre-determined news story layout
	=> Would require uniquely titled widget containers for each template
      |- Relegate widget design and/or containers to two lengths with all equal container heights
	=> This should be a supported function, with a default home page for "out of the box" usage
      		
  |=> Theme customizations to support:
    |-> Header
      |- Optional display of header text
      |- Customizable header text color
      |- Customizable *banner*
	=> Deploy theme with default banner which is replacable by user
    |-> Theme options
      |- multiple color schemes
      |- w/ custom link/text/heading colors
      |- alternate content locations (left, right, w/o sidebar)
      |- Customizable body bg color
    |-> Widgets
      |- LOTS of widgetizable areas
      |- Try to get all the default wordpress widgets in there
      |- Planned widgets
	=> Archives
	=> Links/Blogroll
	=> ?Categories
	=> Recent Posts
	=> Search
	=> Text
	=> Calendar
	=> Custom menu
	=> Meta
	=> Recent Comments
	=> RSS
	=> Tag Cloud
	---------------------
	=> Wide news column - Category based
	  -> Include category filters as widget option
	=> Narrow news column - Category based
	  -> Include category filters as widget option
	=> Wide news column - Tag based
	  -> Include tag filters as widget option
	=> Narrow news column - Tag based
	  -> Include tag filters as widget option

ORDER OF OPERATIONS:

  |-1 Complete graphic design of:
//    |=> Index page w/outlines for widgetized sidebars
//    |=> 3 news page template layouts
//    |=> Header
//    |=> Widgetized footer
//    |=> Navbars
  |-2 Out of the box functionality
    |=> Index page
    |=> Stylesheet
    |=> Functions.php
    |=> Widgets support
      -> Register in functions.php
    |=> Sidebar.php
      |-> Widgetize
      |-> Dual sidebars
    |=> Menus
      |-> Support for sidebar menu
      |-> Support for top menu
    |=> Page.php (static template)
    |=> Header.php
    |=> Footer.php
    |=> Single.php
    |=> 404.php
    |=> Comments
      |-> Design comments area
      |-> Include in single.php
    |=> Archive.php
    |=> Search bar
      |-> Place in layout
      |-> Attempt to style
    |=> Plugin API
      |-> Actual plugin work will be moved to separate project
    |=> Fonts
      |-> Pick something clean & professional looking; rip fonts from NYT if you really want to
  |-3 Extensible functionality
    |=> Basic customizations
      |-> Background color---------------|
      |-> Link/text/heading color--------|
      |-> Alternate color/style scheme---|---> Will collectively rev version by .2 when completed
      |-> Header options
      |-> Content box relocation
    |=> Complex cusomization
      |-> Wiget-based news page template*
      |-> Category-based, plugin controlled news page template*
      *Only one of the two will be implemented, pending development decision

RELEASE SCHEDULE:
  |=> Alpha 0.1
    |-> Version will be revved when first draft of stylesheet is written; signifies the start of significant development.
    |-> After 25% of top tier stage 2 items complete, version will be revved to 0.2. 50% completion will rev to 0.3, etc.
      |- 25% completion: 4 items
      |- 50% completion: 8 items
      |- 75% completion: 12 items
      |- 100% completion: 16 items
  |=> Beta 0.5
    |-> Indicates theme is fully usable and suited for deployment on wordpress blogs
    |-> Version will be revved when all stage 2 items have been completed
    |-> Completion of any stage 3 item will rev version by 0.1 unless otherwise indicated
  |=> Release 1.0
    |-> Indicates full theme release with support for basic customizability
    |-> Version will be revved when all items under "Basic customizations" in stage 3 are complete
  |=> Release 1.5
    |-> Indicates that all items in initial roadmap are complete and tested
    |-> Version will be revved when all items in all 3 stages are complete.
<?php

//Functtion limits all excerpt lengths to selected excerpt length

/*function wp_trim_all_excerpt($text) {
// Creates an excerpt if needed; and shortens the manual excerpt as well
global $post;
  $raw_excerpt = $text;
  if ( '' == $text ) {
    $text = get_the_content('');
    $text = strip_shortcodes( $text );
    $text = apply_filters('the_content', $text);
    $text = str_replace(']]>', ']]&gt;', $text);
  }

$text = strip_tags($text);
$excerpt_length = apply_filters('excerpt_length', 55);
$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
$text = wp_trim_words( $text, $excerpt_length, $excerpt_more ); //since wp3.3
/*$words = explode(' ', $text, $excerpt_length + 1);
  if (count($words)> $excerpt_length) {
    array_pop($words);
    $text = implode(' ', $words);
    $text = $text . $excerpt_more;
  } else {
    $text = implode(' ', $words);
  }
return $text;*/
/*return apply_filters('wp_trim_excerpt', $text, $raw_excerpt); //since wp3.3
}

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'wp_trim_all_excerpt');

//End excerpt limit function*/


//Function allows custom defined excerpt lengths
function my_excerpt($excerpt_length = 55, $id = false, $echo = true) {

    $text = '';

	  if($id) {
	  	$the_post = & get_post( $my_id = $id );
	  	$text = ($the_post->post_excerpt) ? $the_post->post_excerpt : $the_post->post_content;
	  } else {
	  	global $post;
	  	$text = ($post->post_excerpt) ? $post->post_excerpt : get_the_content('');
    }

		$text = strip_shortcodes( $text );
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]&gt;', $text);
	  $text = strip_tags($text);

		$excerpt_more = ' ' . '[...]';
		$words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
		if ( count($words) > $excerpt_length ) {
			array_pop($words);
			$text = implode(' ', $words);
			$text = $text . $excerpt_more;
		} else {
			$text = implode(' ', $words);
		}
	if($echo)
  echo apply_filters('the_content', $text);
	else
	return $text;
}

function get_my_excerpt($excerpt_length = 55, $id = false, $echo = false) {
 return my_excerpt($excerpt_length, $id, $echo);
}
//End excerpt length function

//Post title marquee scroll functions


//Catch first image in a post and display it
// Get URL of first image in a post
function catch_that_image() {
global $post, $posts;
$first_img = '';
ob_start();
ob_end_clean();
$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
$first_img = $matches [1] [0];

// no image found display default image instead
if(empty($first_img)){
$first_img = "/images/default.jpg";
}
return $first_img;
}
//End image catch -- Credit goes to

//Register menus
function register_my_menus() {
  register_nav_menus(
    array(
      'navbar'=>__('Navigation Bar')
    )
  );
}
add_action('init', 'register_my_menus');
//End menu registration

//Post title marquee scroll functions
function ptmsshow()
{
	global $wpdb;
	$ptms_marquee = "";

	@$ptms_scrollamount = get_option('ptms_scrollamount');
	@$ptms_scrolldelay = get_option('ptms_scrolldelay');
	@$ptms_direction = get_option('ptms_direction');
	@$ptms_style = get_option('ptms_style');

	@$ptms_noofpost = get_option('ptms_noofpost');
	@$ptms_categories = get_option('ptms_categories');
	@$ptms_orderbys = get_option('ptms_orderbys');
	@$ptms_order = get_option('ptms_order');
	@$ptms_spliter = get_option('ptms_spliter');

	if(!is_numeric($ptms_scrollamount)){ $ptms_scrollamount = 2; }
	if(!is_numeric($ptms_scrolldelay)){ $ptms_scrolldelay = 5; }
	if(!is_numeric($ptms_noofpost)){ $ptms_noofpost = 10; }

	$sSql = query_posts('cat='.$ptms_categories.'&orderby='.$ptms_orderbys.'&order='.$ptms_order.'&showposts='.$ptms_noofpost);

	if ( ! empty($sSql) )
	{
		@$count = 0;
		foreach ( $sSql as $sSql )
		{
			@$title = stripslashes($sSql->post_title);
			@$link = get_permalink($sSql->ID);
			if($count > 0)
			{
				@$spliter = $ptms_spliter;
			}
			@$ptms = @$ptms . @$spliter . "<a href='".$link."'>" . @$title . "</a>";

			$count = $count + 1;
		}
	}
	wp_reset_query();
	$ptms_marquee = $ptms_marquee . "<div style='padding:3px;' class='ptms_marquee'>";
	$ptms_marquee = $ptms_marquee . "<marquee style='$ptms_style' scrollamount='$ptms_scrollamount' scrolldelay='$ptms_scrolldelay' direction='$ptms_direction' onmouseover='this.stop()' onmouseout='this.start()'>";
	$ptms_marquee = $ptms_marquee . $ptms;
	$ptms_marquee = $ptms_marquee . "</marquee>";
	$ptms_marquee = $ptms_marquee . "</div>";
	echo $ptms_marquee;
}

add_filter('the_content','ptms_show_filter');

function ptms_show_filter($content)
{
	return 	preg_replace_callback('/\[POST-MARQUEE(.*?)\]/sim','ptms_show_filter_callback',$content);
}

function ptms_show_filter_callback($matches)
{

	global $wpdb;
	$ptms_marquee = "";

	@$ptms_scrollamount = get_option('ptms_scrollamount');
	@$ptms_scrolldelay = get_option('ptms_scrolldelay');
	@$ptms_direction = get_option('ptms_direction');
	@$ptms_style = get_option('ptms_style');

	@$ptms_noofpost = get_option('ptms_noofpost');
	@$ptms_categories = get_option('ptms_categories');
	@$ptms_orderbys = get_option('ptms_orderbys');
	@$ptms_order = get_option('ptms_order');
	@$ptms_spliter = get_option('ptms_spliter');

	if(!is_numeric($ptms_scrollamount)){ $ptms_scrollamount = 2; }
	if(!is_numeric($ptms_scrolldelay)){ $ptms_scrolldelay = 5; }
	if(!is_numeric($ptms_noofpost)){ $ptms_noofpost = 10; }

	//$sSql = query_posts('cat='.$ptms_categories.'&orderby='.$ptms_orderbys.'&order='.$ptms_order.'&showposts='.$ptms_noofpost);

	$sSqlMin = "select p.ID, p.post_title, wpr.object_id, ". $wpdb->prefix . "terms.name , ". $wpdb->prefix . "terms.term_id ";
	$sSqlMin = $sSqlMin . "from ". $wpdb->prefix . "terms ";
	$sSqlMin = $sSqlMin . "inner join ". $wpdb->prefix . "term_taxonomy on ". $wpdb->prefix . "terms.term_id = ". $wpdb->prefix . "term_taxonomy.term_id ";
	$sSqlMin = $sSqlMin . "inner join ". $wpdb->prefix . "term_relationships wpr on wpr.term_taxonomy_id = ". $wpdb->prefix . "term_taxonomy.term_taxonomy_id ";
	$sSqlMin = $sSqlMin . "inner join ". $wpdb->prefix . "posts p on p.ID = wpr.object_id ";
	$sSqlMin = $sSqlMin . "where taxonomy= 'category' and p.post_type = 'post' and p.post_status = 'publish'";
	//$sSqlMin = $sSqlMin . "order by object_id; ";

	if( ! empty($ptms_categories) )
	{
		$sSqlMin = $sSqlMin . " and ". $wpdb->prefix . "terms.term_id in($ptms_categories)";
	}

	if( ! empty($ptms_orderbys) )
	{

		if($ptms_orderbys <> "rand" )
		{
			$sSqlMin = $sSqlMin . " order by p.$ptms_orderbys";

			if( ! empty($ptms_order) )
			{
				$sSqlMin = $sSqlMin . " $ptms_order";
			}
		}
		else
		{
			$sSqlMin = $sSqlMin . " order by rand()";
		}

	}

	if( ! empty($ptms_noofpost) )
	{
		$sSqlMin = $sSqlMin . " limit 0, $ptms_noofpost";
	}

	//echo $sSqlMin;

	$sSql = $wpdb->get_results($sSqlMin);

	if ( ! empty($sSql) )
	{
		@$count = 0;
		foreach ( $sSql as $sSql )
		{
			@$title = stripslashes($sSql->post_title);
			@$link = get_permalink($sSql->ID);
			if($count > 0)
			{
				@$spliter = $ptms_spliter;
			}
			@$ptms = @$ptms . @$spliter . "<a href='".$link."'>" . @$title . "</a>";

			$count = $count + 1;
		}
	}
	$ptms_marquee = $ptms_marquee . "<div style='padding:3px;' class='ptms_marquee'>";
	$ptms_marquee = $ptms_marquee . "<marquee style='$ptms_style' scrollamount='$ptms_scrollamount' scrolldelay='$ptms_scrolldelay' direction='$ptms_direction' onmouseover='this.stop()' onmouseout='this.start()'>";
	$ptms_marquee = $ptms_marquee . $ptms;
	$ptms_marquee = $ptms_marquee . "</marquee>";
	$ptms_marquee = $ptms_marquee . "</div>";
	return $ptms_marquee;
}


function ptms_install()
{
	add_option('ptms_title', "Post title marquee scroll");

	add_option('ptms_scrollamount', "2");
	add_option('ptms_scrolldelay', "5");
	add_option('ptms_direction', "left");
	add_option('ptms_style', "color:#FF0000;font:Arial;");

	add_option('ptms_noofpost', "10");
	add_option('ptms_categories', "");
	add_option('ptms_orderbys', "ID");
	add_option('ptms_order', "DESC");
	add_option('ptms_spliter', " - ");
}

function ptms_widget($args)
{
	extract($args);
	if(get_option('ptms_title') <> "")
	{
		echo $before_widget;
		echo $before_title;
		echo get_option('ptms_title');
		echo $after_title;
	}
	ptmsshow();
	if(get_option('ptms_title') <> "")
	{
		echo $after_widget;
	}
}

function ptms_control()
{
	echo "Post title marquee scroll";
}

function ptms_widget_init()
{
	if(function_exists('wp_register_sidebar_widget'))
	{
		wp_register_sidebar_widget('post-title-marquee-scroll', 'Post title marquee scroll', 'ptms_widget');
	}

	if(function_exists('wp_register_widget_control'))
	{
		wp_register_widget_control('post-title-marquee-scroll', array('Post title marquee scroll', 'widgets'), 'ptms_control');
	}
}

function ptms_deactivation()
{

}

function ptms_option()
{
	global $wpdb;
	echo '<h2>Post title marquee scroll</h2>';

	$ptms_title = get_option('ptms_title');

	$ptms_scrollamount = get_option('ptms_scrollamount');
	$ptms_scrolldelay = get_option('ptms_scrolldelay');
	$ptms_direction = get_option('ptms_direction');
	$ptms_style = get_option('ptms_style');

	$ptms_noofpost = get_option('ptms_noofpost');
	$ptms_categories = get_option('ptms_categories');
	$ptms_orderbys = get_option('ptms_orderbys');
	$ptms_order = get_option('ptms_order');
	$ptms_spliter = get_option('ptms_spliter');

	if (@$_POST['ptms_submit'])
	{
		$ptms_title = stripslashes($_POST['ptms_title']);

		$ptms_scrollamount = stripslashes($_POST['ptms_scrollamount']);
		$ptms_scrolldelay = stripslashes($_POST['ptms_scrolldelay']);
		$ptms_direction = stripslashes($_POST['ptms_direction']);
		$ptms_style = stripslashes($_POST['ptms_style']);

		$ptms_noofpost = stripslashes($_POST['ptms_noofpost']);
		$ptms_categories = stripslashes($_POST['ptms_categories']);
		$ptms_orderbys = stripslashes($_POST['ptms_orderbys']);
		$ptms_order = stripslashes($_POST['ptms_order']);
		$ptms_spliter = stripslashes($_POST['ptms_spliter']);

		update_option('ptms_title', $ptms_title );

		update_option('ptms_scrollamount', $ptms_scrollamount );
		update_option('ptms_scrolldelay', $ptms_scrolldelay );
		update_option('ptms_direction', $ptms_direction );
		update_option('ptms_style', $ptms_style );

		update_option('ptms_noofpost', $ptms_noofpost );
		update_option('ptms_categories', $ptms_categories );
		update_option('ptms_orderbys', $ptms_orderbys );
		update_option('ptms_order', $ptms_order );
		update_option('ptms_spliter', $ptms_spliter );
	}

	echo '<form name="ptms_form" method="post" action="">';

	echo '<p>Title :<br><input  style="width: 250px;" type="text" value="';
	echo $ptms_title . '" name="ptms_title" id="ptms_title" /></p>';

	echo '<p>Scroll amount :<br><input  style="width: 100px;" type="text" value="';
	echo $ptms_scrollamount . '" name="ptms_scrollamount" id="ptms_scrollamount" /></p>';

	echo '<p>Scroll delay :<br><input  style="width: 100px;" type="text" value="';
	echo $ptms_scrolldelay . '" name="ptms_scrolldelay" id="ptms_scrolldelay" /></p>';

	echo '<p>Scroll direction :<br><input  style="width: 100px;" type="text" value="';
	echo $ptms_direction . '" name="ptms_direction" id="ptms_direction" /> (Left/Right)</p>';

	echo '<p>Scroll style :<br><input  style="width: 250px;" type="text" value="';
	echo $ptms_style . '" name="ptms_style" id="ptms_style" /></p>';

	echo '<p>Spliter :<br><input  style="width: 100px;" type="text" value="';
	echo $ptms_spliter . '" name="ptms_spliter" id="ptms_spliter" /></p>';

	echo '<p>Number of post :<br><input  style="width: 100px;" type="text" value="';
	echo $ptms_noofpost . '" name="ptms_noofpost" id="ptms_noofpost" /></p>';

	echo '<p>Post categories :<br><input  style="width: 200px;" type="text" value="';
	echo $ptms_categories . '" name="ptms_categories" id="ptms_categories" /> (Example: 1, 3, 4) <br> Category IDs, separated by commas.</p>';

	echo '<p>Post orderbys :<br><input  style="width: 200px;" type="text" value="';
	echo $ptms_orderbys . '" name="ptms_orderbys" id="ptms_orderbys" /> (Any 1 from below list) <br> ID/author/title/rand/date/category/modified</p>';

	echo '<p>Post order : <br><input  style="width: 100px;" type="text" value="';
	echo $ptms_order . '" name="ptms_order" id="ptms_order" /> ASC/DESC </p>';

	echo '<input name="ptms_submit" id="ptms_submit" lang="publish" class="button-primary" value="Update" type="Submit" />';
	echo '</form>';
	?>
    <h2>Plugin configuration help</h2>
    <ul>
    	<li>Drag and drop the widget</a></li>
        <li>Short code for posts and pages</a></li>
        <li>Add directly in the theme</li>
    </ul>
    <h2>Check official website</h2>
    <ul>
    	<li>Check official website for more info <a href="http://www.gopiplus.com/work/2011/08/08/post-title-marquee-scroll-wordpress-plugin/" target="_blank">Click here</a></li>
    </ul>
    <?php
}

function ptms_add_to_menu()
{
	add_options_page('Post title marquee scroll', 'Post title marquee scroll', 'manage_options', __FILE__, 'ptms_option' );
}

if (is_admin())
{
	add_action('admin_menu', 'ptms_add_to_menu');
}

add_action("plugins_loaded", "ptms_widget_init");
register_activation_hook(__FILE__, 'ptms_install');
register_deactivation_hook(__FILE__, 'ptms_deactivation');
add_action('init', 'ptms_widget_init');
//End post title marquee scroll functions
//Credit for marquee to Gopi.R
//Plugin URI: http://www.gopiplus.com/work/2011/08/08/post-title-marquee-scroll-wordpress-plugin/


?>
<?php get_header(); ?>

<div class="news-wide">
  <?php query_posts('posts_per_page=1') ?>
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="wide-main">
      <div class="main-left">
	<a href="<?php the_permalink(); ?>"><img src="<?php catch_that_image(); ?>" class="news-main-img" /></a>
      </div><!--End main left-->
      <div class="main-right">
	<div class="main-title">
	  <h1 class="main-header"><a href="<?php the_permalink(); ?>" title="Link to full story"><?php the_title(); ?></a></h1>
	</div><!--End main title-->
	<div class="main-auth-date">
	  <p>by <?php the_author_posts_link(); ?> on <?php the_time('F jS Y'); ?></p>
	</div><!--End wide date auth-->
      </div><!--End main right-->
      <div class="main-bottom">
	<p class=main-content"><?php my_excerpt(150) ?> <a href="<?php the_permalink(); ?>" title="Read full story">Read full story &lt;&lt;<?php the_title(); ?>&gt;&gt;</p>
      </div><!--End main bottom-->
    </div><!--End wide main-->
  <?php endwhile; else : ?>
    <div class="wide-main">
      <p>No news for this section.</p>
    </div><!--End wide main-->
  <?php endif; wp_reset_query(); ?>
  <?php query_posts('posts_per_page=2&offset=1');
  if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="wide-sub">
      <div class="sub-left">
	<a href="<?php the_permalink(); ?>"><img src="<?php catch_that_image(); ?>" class="news-sub-img" /></a>
      </div><!--End sub left-->
      <div class="sub-right">
	<div class="sub-title">
	  <h4 class="sub-header"><a href="<?php the_permalink(); ?>" title="Link to full story"><?php the_title(); ?></a></h4>
	</div><!--End sub title-->
	<div class="sub-auth-date">
	  <p>by <?php the_author_posts_link(); ?> on <?php the_time('F jS Y'); ?></p>
	</div><!--End sub auth date-->
	<div class="sub-bottom">
	  <p class="sub-content"><?php my_excerpt(40); ?> <a href="<?php the_permalink(); ?>" title="Read full story">Read full story &lt;&lt;<?php the_title(); ?>&gt;&gt;</p>
	</div>
      </div><!--End sub right-->
    </div><!--End wide sub-->
  <?php endwhile; else : ?>
    <div class="wide-sub">
      <p>No news for this section.</p>
    </div>
  <?php endif; wp_reset_query(); ?>
</div><!--End news wide-->

<div class="news-narrow">
  <?php query_posts('posts_per_page=3&offset=3') ?>
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="news-narrow-post">
      <h3 class="narrow-title"><?php the_title(); ?></h3>
      <div class="narrow-auth-date">
	<p>by <?php the_author_posts_link(); ?> on <?php the_time('F jS Y'); ?></p>
      </div>
      <p class="narrow-content"><?php my_excerpt(55); ?> <a href="<?php the_permalink(); ?>" title="Read full story">Read full story &lt;&lt;<?php the_title ?>&gt;&gt;</p>
  <?php endwhile; else : ?>
      <p>No news for this section.</p>
  <?php endif; ?>
    </div><!--End news narrow post-->
    <?php wp_reset_query(); ?>
</div><!--End news narrow-->
<p>Try Me!</p>

<div class="bottom-widget-bar">
  <?php dynamic_sidebar('lowbar'); ?>
</div><!--End bottom widget bar-->

<p>Now try me!</p>

<?php get_footer(); ?>
wp-steignbaugh
==============

Custom Wordpress theme designed for adamsteinbaugh.com

DEVELOPMENT NEWS:

2012-08-24, 0027 hrs
Graphic design of header and index has been completed. Staging phase is considered as having reached completion; coding to begin over the next 24 hours.

2012-08-23
Theme is in the staging phase. Roadmap of theme work has been completed and loaded into the TODO file, tasks will begin to be loaded into the issues bar as staging comes to a close.

This commit added a LICENSE file as well as a completed road map in the TODO file.